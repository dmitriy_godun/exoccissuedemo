/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arkha.alto.clearappdemo.exoplayer

import android.app.Dialog
import android.content.DialogInterface
import android.content.res.Resources
import android.os.Bundle
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.arkha.alto.clearappdemo.R
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector.SelectionOverride
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo
import com.google.android.exoplayer2.ui.TrackSelectionView
import com.google.android.exoplayer2.ui.TrackSelectionView.TrackSelectionListener
import com.google.android.exoplayer2.util.Assertions
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.track_selection_dialog.view.*
import java.util.*

class TrackSelectionDialog : DialogFragment() {
    private val tabFragments: SparseArray<TrackSelectionViewFragment> = SparseArray()
    private val tabTrackTypes: ArrayList<Int> = ArrayList()
    private var titleId = 0
    private var onClickListener: DialogInterface.OnClickListener? = null
    private var onDismissListener: DialogInterface.OnDismissListener? = null

    private fun init(
            titleId: Int,
            mappedTrackInfo: MappedTrackInfo,
            initialParameters: DefaultTrackSelector.Parameters,
            allowAdaptiveSelections: Boolean,
            allowMultipleOverrides: Boolean,
            onClickListener: DialogInterface.OnClickListener,
            onDismissListener: DialogInterface.OnDismissListener) {
        this.titleId = titleId
        this.onClickListener = onClickListener
        this.onDismissListener = onDismissListener
        for (i in 0 until mappedTrackInfo.rendererCount) {
            if (showTabForRenderer(mappedTrackInfo, i)) {
                val trackType = mappedTrackInfo.getRendererType(i)
                val trackGroupArray = mappedTrackInfo.getTrackGroups(i)
                val tabFragment = TrackSelectionViewFragment()
                tabFragment.init(
                        mappedTrackInfo,
                        i,
                        initialParameters.getRendererDisabled(i),
                        initialParameters.getSelectionOverride(i, trackGroupArray),
                        allowAdaptiveSelections,
                        allowMultipleOverrides)
                tabFragments.put(i, tabFragment)
                tabTrackTypes.add(trackType)
            }
        }
    }

    fun getIsDisabled(rendererIndex: Int): Boolean {
        val rendererView = tabFragments[rendererIndex]
        return rendererView != null && rendererView.isDisabled
    }

    fun getOverrides(rendererIndex: Int): List<SelectionOverride> {
        val rendererView = tabFragments[rendererIndex]
        return rendererView?.overrides ?: emptyList()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AppCompatDialog(activity, R.style.TrackSelectionDialogThemeOverlay)
        dialog.setTitle(titleId)
        return dialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismissListener?.onDismiss(dialog)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dialogView = inflater.inflate(R.layout.track_selection_dialog, container, false)
        val tabLayout: TabLayout = dialogView.track_selection_dialog_tab_layout
        val viewPager: ViewPager = dialogView.track_selection_dialog_view_pager
        val cancelButton = dialogView.track_selection_dialog_cancel_button
        val okButton = dialogView.track_selection_dialog_ok_button
        viewPager.adapter = FragmentAdapter(childFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.visibility = if (tabFragments.size() > 1) View.VISIBLE else View.GONE
        cancelButton.setOnClickListener { dismiss() }
        okButton.setOnClickListener {
            onClickListener?.onClick(dialog, DialogInterface.BUTTON_POSITIVE)
            dismiss()
        }
        return dialogView
    }

    private inner class FragmentAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        override fun getItem(position: Int): Fragment {
            return tabFragments.valueAt(position)
        }

        override fun getCount(): Int {
            return tabFragments.size()
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return getTrackTypeString(resources, tabTrackTypes[position])
        }
    }

    class TrackSelectionViewFragment : Fragment(), TrackSelectionListener {
        private lateinit var mappedTrackInfo: MappedTrackInfo
        private var rendererIndex = 0
        private var allowAdaptiveSelections = false
        private var allowMultipleOverrides = false

        var isDisabled = false

        lateinit var overrides: List<SelectionOverride>
        fun init(
                mappedTrackInfo: MappedTrackInfo,
                rendererIndex: Int,
                initialIsDisabled: Boolean,
                initialOverride: SelectionOverride?,
                allowAdaptiveSelections: Boolean,
                allowMultipleOverrides: Boolean) {
            this.mappedTrackInfo = mappedTrackInfo
            this.rendererIndex = rendererIndex
            isDisabled = initialIsDisabled
            overrides = initialOverride?.let { listOf(it) } ?: emptyList()
            this.allowAdaptiveSelections = allowAdaptiveSelections
            this.allowMultipleOverrides = allowMultipleOverrides
        }

        override fun onCreateView(
                inflater: LayoutInflater,
                container: ViewGroup?,
                savedInstanceState: Bundle?): View? {
            val rootView = inflater.inflate(
                    R.layout.exo_track_selection_dialog, container, false)
            val trackSelectionView: TrackSelectionView = rootView.findViewById(R.id.exo_track_selection_view)
            trackSelectionView.setShowDisableOption(true)
            trackSelectionView.setAllowMultipleOverrides(allowMultipleOverrides)
            trackSelectionView.setAllowAdaptiveSelections(allowAdaptiveSelections)
            trackSelectionView.init(
                    mappedTrackInfo, rendererIndex, isDisabled, overrides, this)
            return rootView
        }

        override fun onTrackSelectionChanged(isDisabled: Boolean, overrides: List<SelectionOverride>) {
            this.isDisabled = isDisabled
            this.overrides = overrides
        }

        init {
            retainInstance = true
        }
    }

    companion object {

        fun willHaveContent(trackSelector: DefaultTrackSelector): Boolean {
            val mappedTrackInfo = trackSelector.currentMappedTrackInfo
            return mappedTrackInfo != null && willHaveContent(mappedTrackInfo)
        }

        fun willHaveContent(mappedTrackInfo: MappedTrackInfo): Boolean {
            for (i in 0 until mappedTrackInfo.rendererCount) {
                if (showTabForRenderer(mappedTrackInfo, i)) {
                    return true
                }
            }
            return false
        }

        fun createForTrackSelector(
                trackSelector: DefaultTrackSelector, onDismissListener: DialogInterface.OnDismissListener): TrackSelectionDialog {
            val mappedTrackInfo = Assertions.checkNotNull(trackSelector.currentMappedTrackInfo)
            val trackSelectionDialog = TrackSelectionDialog()
            val parameters = trackSelector.parameters
            trackSelectionDialog.init(
                    R.string.track_selection_title,
                    mappedTrackInfo,
                    parameters,
                    true,
                    false,
                    DialogInterface.OnClickListener { _, _ ->
                        val builder = parameters.buildUpon()
                        for (i in 0 until mappedTrackInfo.rendererCount) {
                            builder.clearSelectionOverrides(i)
                                    .setRendererDisabled(i, trackSelectionDialog.getIsDisabled(i))
                            val overrides = trackSelectionDialog.getOverrides(i)
                            if (overrides.isNotEmpty()) {
                                builder.setSelectionOverride(
                                        i,
                                        mappedTrackInfo.getTrackGroups(i),
                                        overrides[0])
                            }
                        }
                        trackSelector.setParameters(builder)
                    },
                    onDismissListener)
            return trackSelectionDialog
        }

        fun createForMappedTrackInfoAndParameters(
                titleId: Int,
                mappedTrackInfo: MappedTrackInfo,
                initialParameters: DefaultTrackSelector.Parameters,
                allowAdaptiveSelections: Boolean,
                allowMultipleOverrides: Boolean,
                onClickListener: DialogInterface.OnClickListener,
                onDismissListener: DialogInterface.OnDismissListener): TrackSelectionDialog {
            val trackSelectionDialog = TrackSelectionDialog()
            trackSelectionDialog.init(
                    titleId,
                    mappedTrackInfo,
                    initialParameters,
                    allowAdaptiveSelections,
                    allowMultipleOverrides,
                    onClickListener,
                    onDismissListener)
            return trackSelectionDialog
        }

        private fun showTabForRenderer(mappedTrackInfo: MappedTrackInfo, rendererIndex: Int): Boolean {
            val trackGroupArray = mappedTrackInfo.getTrackGroups(rendererIndex)
            if (trackGroupArray.length == 0) {
                return false
            }
            val trackType = mappedTrackInfo.getRendererType(rendererIndex)
            return isSupportedTrackType(trackType)
        }

        private fun isSupportedTrackType(trackType: Int): Boolean {
            return when (trackType) {
                C.TRACK_TYPE_VIDEO, C.TRACK_TYPE_AUDIO, C.TRACK_TYPE_TEXT -> true
                else -> false
            }
        }

        private fun getTrackTypeString(resources: Resources, trackType: Int): String {
            return when (trackType) {
                C.TRACK_TYPE_VIDEO -> resources.getString(R.string.exo_track_selection_title_video)
                C.TRACK_TYPE_AUDIO -> resources.getString(R.string.exo_track_selection_title_audio)
                C.TRACK_TYPE_TEXT -> resources.getString(R.string.exo_track_selection_title_text)
                else -> throw IllegalArgumentException()
            }
        }
    }

    init {
        retainInstance = true
    }
}