/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arkha.alto.clearappdemo.exoplayer.datasource;

import android.content.Context;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSource.Factory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;

/**
 * A {@link Factory} that produces {@link ExoDataSource} instances that delegate to
 * {@link DefaultHttpDataSource}s for non-file/asset/content URIs.
 */
public final class ExoDataSourceFactory implements Factory {

    private final Context context;
    @Nullable
    private final TransferListener listener;
    private final Factory baseDataSourceFactory;

    /**
     * @param context   A context.
     * @param userAgent The User-Agent string that should be used.
     */
    public ExoDataSourceFactory(Context context, String userAgent) {
        this(context, userAgent, /* listener= */ null);
    }

    /**
     * @param context   A context.
     * @param userAgent The User-Agent string that should be used.
     * @param listener  An optional listener.
     */
    public ExoDataSourceFactory(
            Context context, String userAgent, @Nullable TransferListener listener) {
        this(context, listener, new DefaultHttpDataSourceFactory(userAgent, listener));
    }

    /**
     * @param context               A context.
     * @param baseDataSourceFactory A {@link Factory} to be used to create a base {@link DataSource}
     *                              for {@link ExoDataSource}.
     * @see ExoDataSource#ExoDataSource(Context, DataSource)
     */
    public ExoDataSourceFactory(Context context, DataSource.Factory baseDataSourceFactory) {
        this(context, /* listener= */ null, baseDataSourceFactory);
    }

    /**
     * @param context               A context.
     * @param listener              An optional listener.
     * @param baseDataSourceFactory A {@link Factory} to be used to create a base {@link DataSource}
     *                              for {@link ExoDataSource}.
     * @see ExoDataSource#ExoDataSource(Context, DataSource)
     */
    public ExoDataSourceFactory(
            Context context,
            @Nullable TransferListener listener,
            DataSource.Factory baseDataSourceFactory) {
        this.context = context.getApplicationContext();
        this.listener = listener;
        this.baseDataSourceFactory = baseDataSourceFactory;
    }

    @Override
    public ExoDataSource createDataSource() {
        ExoDataSource dataSource =
                new ExoDataSource(context, baseDataSourceFactory.createDataSource());
        if (listener != null) {
            dataSource.addTransferListener(listener);
        }
        return dataSource;
    }
}
