package com.arkha.alto.clearappdemo.exoplayer

import android.view.View
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector

interface MultimediaPlayer {
    fun setStateListener(stateListener: PlayerStateListener)

    fun removeStateListener()

    fun attachToView(view: View)

    fun detach(view: View)

    fun onLoad(url: String)

    fun onLoad(url: String, playWhenReady: Boolean)

    fun onStart()

    fun play()

    fun onResume()

    fun pause()

    fun onPause()

    fun onRefresh()

    fun onStop()

    fun onRestoreState()

    fun muteSound(mute: Boolean)

    fun changePlayState()

    fun getCurrentVideoPosition(): Int?

    fun updateTrackParams(params: DefaultTrackSelector.Parameters)

    fun getDefaultTrackSelector(): DefaultTrackSelector

}