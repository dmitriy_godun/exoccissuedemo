package com.arkha.alto.clearappdemo

import android.os.Build
import com.arkha.alto.clearappdemo.BuildConfig
import java.util.regex.Pattern

object NetworkUtils {
    private const val PATTERN = "Alto/%s/%s (Device: %s %s; Android %s)"
    private val userAgent: String

    init {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        val androidVersion = Build.VERSION.RELEASE
        val versionCode = BuildConfig.VERSION_CODE
        val versionName = BuildConfig.VERSION_NAME

        userAgent = String.format(
            PATTERN, versionName, versionCode,
            manufacturer, model, androidVersion
        )
    }

    fun getUserAgent() = userAgent
}

object CommonUtils {
    fun getYouTubeVideoUrl(media_url: String?): String {
        var url = media_url
        val expression =
            "^.*((youtu.be" + "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"
        val matcher = Pattern.compile(expression, Pattern.CASE_INSENSITIVE).matcher(media_url)
        if (matcher.matches()) {
            val groupIndex = matcher.group(7)
            if (groupIndex != null && groupIndex.length == 11)
                url = groupIndex
        }
        return url ?: ""
    }

}