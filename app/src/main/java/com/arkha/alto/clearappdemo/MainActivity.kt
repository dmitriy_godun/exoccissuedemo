package com.arkha.alto.clearappdemo

import android.os.Bundle
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.arkha.alto.clearappdemo.exoplayer.ExoVideoPlayer
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), ValueCallback<Boolean> {
    private val exoPlayer: ExoVideoPlayer by lazy { ExoVideoPlayer(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        exoPlayer.attachToView(player_view)

        val intent = intent

        val dataUri = intent.getStringExtra("URL")

        if (dataUri != null) {
            exoPlayer.onLoad(dataUri)
        } else {
            exoPlayer.onLoad("udp://@238.1.1.1:5000")
        }
        btnCC.setOnClickListener {
            exoPlayer.showCaptions(supportFragmentManager)
        }
    }

    override fun onStart() {
        super.onStart()
        exoPlayer.onStart()
    }

    override fun onResume() {
        super.onResume()
        exoPlayer.onResume()
    }

    override fun onPause() {
        super.onPause()
        exoPlayer.onPause()
    }

    override fun onStop() {
        super.onStop()
        exoPlayer.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        exoPlayer.detach(player_view)
    }

    override fun onReceiveValue(p0: Boolean?) {

    }
}