package com.arkha.alto.clearappdemo.exoplayer

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.net.Uri
import android.util.Log
import android.util.SparseArray
import android.view.View
import androidx.fragment.app.FragmentManager
import at.huber.youtubeExtractor.VideoMeta
import at.huber.youtubeExtractor.YouTubeExtractor
import at.huber.youtubeExtractor.YtFile
import com.arkha.alto.clearappdemo.CommonUtils
import com.arkha.alto.clearappdemo.NetworkUtils
import com.arkha.alto.clearappdemo.R
import com.arkha.alto.clearappdemo.exoplayer.datasource.ExoDataSourceFactory
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager
import com.google.android.exoplayer2.drm.ExoMediaDrm
import com.google.android.exoplayer2.drm.FrameworkMediaDrm
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.extractor.mp4.Mp4Extractor
import com.google.android.exoplayer2.extractor.ts.DefaultTsPayloadReaderFactory
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil
import com.google.android.exoplayer2.offline.FilteringManifestParser
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.dash.manifest.DashManifestParser
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsManifestParser
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.EventLogger
import com.google.android.exoplayer2.util.Util

open class ExoVideoPlayer(private val context: Context) : MultimediaPlayer, Player.EventListener {

    private var bandwidthMeter = DefaultBandwidthMeter.Builder(context).build()

    private var stateListener: PlayerStateListener? = null
    private var currentUrl: String = ""
    private var currentAttachedView: View? = null

    private var isPlayWhenReady = true
    private var currentVolume = 0f

    private val drmCallback = HttpMediaDrmCallback(
        DRM_LICENSE_URL,
        DefaultHttpDataSourceFactory(USER_AGENT)
    )

    private val defaultDrmSessionManager = DefaultDrmSessionManager.Builder()
        .setUuidAndExoMediaDrmProvider(
            C.WIDEVINE_UUID,
            ExoMediaDrm.AppManagedProvider(FrameworkMediaDrm.newInstance(C.WIDEVINE_UUID))
        )
        .build(drmCallback)

    override fun setStateListener(stateListener: PlayerStateListener) {
        this.stateListener = stateListener
    }

    override fun removeStateListener() {
        stateListener = null
    }

    private lateinit var trackSelector: DefaultTrackSelector
    private lateinit var loadControl: LoadControl
    private lateinit var rendererFactory: RenderersFactory

    private var player: SimpleExoPlayer? = null
    private var isPause = false

    override fun onPlayerError(error: ExoPlaybackException) {
        if (error != null) {
            when (error.type) {
                ExoPlaybackException.TYPE_SOURCE -> {
                    if (error.sourceException !is HttpDataSource.HttpDataSourceException) {
                        reportError(currentUrl, error.sourceException)
                    }
                }
                ExoPlaybackException.TYPE_RENDERER -> {
                    val cause: Exception? = error.rendererException
                    if (cause is MediaCodecRenderer.DecoderInitializationException) {
                        reportError(
                            when {
                                cause.cause is MediaCodecUtil.DecoderQueryException -> context.getString(
                                    R.string.error_querying_decoders
                                )
                                cause.secureDecoderRequired -> context.getString(
                                    R.string.error_no_secure_decoder,
                                    cause.mimeType
                                )
                                else -> context.getString(
                                    R.string.error_no_decoder,
                                    cause.mimeType
                                )
                            } + " $currentUrl", cause
                        )
                    } else {
                        val exception = cause ?: Exception("RENDERER")
                        reportError(currentUrl, exception)
                    }
                }
                ExoPlaybackException.TYPE_UNEXPECTED -> {
                    val exception = error.unexpectedException
                    reportError(currentUrl, exception)
                }
                ExoPlaybackException.TYPE_OUT_OF_MEMORY -> {
                    reportError(currentUrl, error.fillInStackTrace())
                }
                ExoPlaybackException.TYPE_REMOTE -> {
                    reportError(currentUrl, error.fillInStackTrace())
                }
            }
        } else {
            reportError(currentUrl, Exception(currentUrl))
        }
        stateListener?.onError()
        stateListener?.onError(error.message ?: "")
    }

    private fun reportError(errorMessage: String, exception: Throwable) {
        Log.e("ExpoPlayer", "reportError: $errorMessage", exception)
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        super.onPlayerStateChanged(playWhenReady, playbackState)

        when (playbackState) {
            Player.STATE_ENDED -> {
                stateListener?.onFinishState()
            }
            Player.STATE_READY -> {
                if (playWhenReady.not()) {
                    stateListener?.onStartState()
                } else {
                    stateListener?.onVideoStarted()
                }
            }
            Player.STATE_BUFFERING -> {
                stateListener?.onIdleState()
            }
            Player.STATE_IDLE -> {
                stateListener?.onIdleState()
            }
            else -> {
                // ignore state
            }
        }
    }

    /*override fun onPlaybackStateChanged(state: Int) {
        super.onPlaybackStateChanged(state)

        when (state) {
            Player.STATE_ENDED -> {
                stateListener?.onFinishState()
            }
            Player.STATE_READY -> {

            }
            Player.STATE_BUFFERING -> {
                stateListener?.onIdleState()
            }
            Player.STATE_IDLE -> {
                stateListener?.onIdleState()
            }
            else -> {
                // ignore state
            }
        }
    }*/

    private fun configurePlayer(player: Player) {
        with(player) {
            addListener(this@ExoVideoPlayer)
            playWhenReady = false
        }
    }

    private fun createDataSourceFactory(
        context: Context,
        userAgent: String,
        listener: TransferListener
    ): DataSource.Factory {
        val defaultHttpDataSourceFactory = DefaultHttpDataSourceFactory(
            userAgent, listener,
            DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
            DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
            true
        )

        return ExoDataSourceFactory(context, listener, defaultHttpDataSourceFactory)
    }

    private fun createMediaDataSourceFactory() = createDataSourceFactory(
        context, Util.getUserAgent(context, NetworkUtils.getUserAgent()),
        bandwidthMeter as TransferListener
    )

    private fun buildMediaSource(uri: Uri, overrideExtension: String?): MediaSource {
        val mediaDataSourceFactory = createMediaDataSourceFactory()
        return when (@C.ContentType val type = Util.inferContentType(uri, overrideExtension)) {
            C.TYPE_DASH -> {
                DashMediaSource.Factory(
                    DefaultDashChunkSource.Factory(
                        mediaDataSourceFactory
                    ), mediaDataSourceFactory
                )
                    .setManifestParser(FilteringManifestParser(DashManifestParser(), null))
                    .setDrmSessionManager(defaultDrmSessionManager)
                    .createMediaSource(uri)
            }
            C.TYPE_SS -> {
                SsMediaSource.Factory(
                    DefaultSsChunkSource.Factory(mediaDataSourceFactory),
                    mediaDataSourceFactory
                )
                    .setManifestParser(FilteringManifestParser(SsManifestParser(), null))
                    .setDrmSessionManager(defaultDrmSessionManager)
                    .createMediaSource(uri)
            }
            C.TYPE_HLS -> {
                HlsMediaSource.Factory(mediaDataSourceFactory)
                    .setDrmSessionManager(defaultDrmSessionManager)
                    .createMediaSource(uri)
            }
            C.TYPE_OTHER -> {
                ProgressiveMediaSource.Factory(
                    mediaDataSourceFactory,
                    DefaultExtractorsFactory()
                        .setTsExtractorFlags(DefaultTsPayloadReaderFactory.FLAG_ALLOW_NON_IDR_KEYFRAMES)
                        .setMp4ExtractorFlags(Mp4Extractor.FLAG_WORKAROUND_IGNORE_EDIT_LISTS)
                )
                    .setDrmSessionManager(defaultDrmSessionManager)
                    .createMediaSource(uri)
            }
            else -> {
                throw  IllegalStateException("Unsupported type: $type")
            }
        }
    }

    private fun initializePlayer() {
        if (player == null) {
            rendererFactory = DefaultRenderersFactory(context).apply {
                setExtensionRendererMode(DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF)
            }
            trackSelector = DefaultTrackSelector(context, AdaptiveTrackSelection.Factory()).also {
                it.parameters = DefaultTrackSelector.ParametersBuilder(context)
                    .setMaxVideoSizeSd()
                    .setPreferredAudioLanguage("en")
                    .build()
            }
            loadControl = DefaultLoadControl.Builder().createDefaultLoadControl()
            player = SimpleExoPlayer.Builder(context, rendererFactory)
                .setTrackSelector(trackSelector)
                .setLoadControl(loadControl)
                .build()
                .also { player ->
                    configurePlayer(player)
                }
            player?.addAnalyticsListener(EventLogger(trackSelector))
        }
    }

    private fun releasePlayer() {
        player?.removeListener(this)
        player?.release()
        player = null
    }

    @SuppressLint("StaticFieldLeak")
    private fun checkForYoutubeUrl() {
        try {
            if ((currentUrl.contains("://youtube/") || currentUrl.contains("youtube.com/watch?v=")) || currentUrl.contains(
                    "youtu.be"
                )
            ) {
                val url = CommonUtils.getYouTubeVideoUrl(currentUrl)
                object : YouTubeExtractor(context) {
                    public override fun onExtractionComplete(
                        ytFiles: SparseArray<YtFile>?,
                        vMeta: VideoMeta
                    ) {
                        if (ytFiles != null) {
                            val itag = 22
                            currentUrl = ytFiles.get(itag).url
                            prepareToPlay()
                        }
                        if (ytFiles == null) {
                            return
                        }
                    }
                }.extract(url, true, true)
            } else {
                prepareToPlay()
            }
        } catch (e: Exception) {
            reportError(e.message ?: "", e)
            stateListener?.onError()
        }
    }

    override fun onLoad(url: String, playWhenReady: Boolean) {
        currentUrl = url
    }

    override fun changePlayState() {
        val playerState = player?.playWhenReady ?: false
        player?.playWhenReady = !playerState
    }

    private fun prepareToPlay() {
        /*player?.apply {
            isPlayWhenReady = playWhenReady
            this.playWhenReady = playWhenReady
            val playbackUri = Uri.parse(currentUrl)
            val mediaSource = buildMediaSource(
                playbackUri, playbackUri.path?.substring(
                    (playbackUri.path?.lastIndexOf(".") ?: -1) + 1
                )
            )
            setMediaSource(mediaSource)
            prepare()
        }
        play()*/

        player?.apply {
            isPlayWhenReady = playWhenReady
            this.playWhenReady = playWhenReady
            val playbackUri = Uri.parse(currentUrl)
            buildMediaSource(
                playbackUri, playbackUri.path?.substring(
                    (playbackUri.path?.lastIndexOf(".") ?: -1) + 1
                )
            ).also {
                prepare(it)
            }
        }
        play()
    }

    override fun onStart() {
        initializePlayer()
    }

    override fun attachToView(view: View) {
        currentAttachedView?.let {
            detach(it)
        }
        if (view is PlayerView) {
            currentAttachedView = view
            view.player = player
        }
    }

    override fun detach(view: View) {
        if (view is PlayerView && view.player != null) {
            view.player = null
        }
    }

    override fun onLoad(url: String) {
        val playWhenReady = if (isPause) {
            isPause = false
            false
        } else {
            true
        }
        onLoad(url, playWhenReady)
    }

    override fun play() {
        player?.let {
            it.seekTo(0)
            currentVolume = it.volume
            it.playWhenReady = true
        }
    }

    override fun onResume() {
        player?.playbackState
        if (currentUrl.isNotEmpty()) {
            onLoad(currentUrl, false)
            checkForYoutubeUrl()
            currentAttachedView?.let {
                attachToView(it)
            }
        }
        isPause = false
    }

    override fun pause() {
        player?.let {
            it.playWhenReady = false
            it.playbackState
        }
    }

    override fun onPause() {
        isPause = true
        pause()
    }

    override fun onRefresh() {
        player?.seekTo(0)
    }

    override fun onStop() {
        releasePlayer()
    }

    override fun onRestoreState() {
        currentUrl = ""
    }

    override fun muteSound(mute: Boolean) {
        if (mute) {
            player?.volume = 0f
        } else {
            player?.volume = currentVolume
        }
    }

    override fun getCurrentVideoPosition(): Int? = player?.currentPosition?.toInt()

    override fun updateTrackParams(params: DefaultTrackSelector.Parameters) {
        trackSelector.parameters = params
    }

    override fun getDefaultTrackSelector(): DefaultTrackSelector = trackSelector

    companion object {
        private const val DRM_LICENSE_URL = "https://widevine-proxy.appspot.com/proxy"
        private const val USER_AGENT = "user-agent"
    }

    fun showCaptions(manager: FragmentManager) {
        trackSelector.let { trackSelector ->
            if (TrackSelectionDialog.willHaveContent(trackSelector)) {
                val trackSelectionDialog: TrackSelectionDialog =
                    TrackSelectionDialog.createForTrackSelector(
                        trackSelector,
                        DialogInterface.OnDismissListener { })
                trackSelectionDialog.show(manager, null)
            }
        }
    }
}