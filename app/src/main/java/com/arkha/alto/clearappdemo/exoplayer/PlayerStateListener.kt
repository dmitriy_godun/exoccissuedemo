package com.arkha.alto.clearappdemo.exoplayer

interface PlayerStateListener {
    fun onVideoStarted()

    fun onFinishState()

    fun onStartState()

    fun onIdleState()

    fun onError()

    fun onError(error: String)
}